import setuptools

# from . import __version__

with open('requirements.txt', 'rt') as f:
    install_requires = [ln.strip() for ln in f.readlines()]

setuptools.setup(
    name="pydr",
    version='0.1.0dev0',
    author="Sean Fitzgibbon",
    author_email="sean.fitzgibbon@ndcn.ox.ac.uk",
    description="Python implementation of dual-regression for CIFTI",
    url="https://git.fmrib.ox.ac.uk/seanf/pydr",
    install_requires=install_requires,
    packages=setuptools.find_packages(),
    include_package_data=True,
    python_requires='>=3.7',
    scripts=['pydr/dr.py', 'pydr/dr_helper.py'],
)